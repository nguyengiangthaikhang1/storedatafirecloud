"use strict";

const firebase = require("../db");
const Student = require("../models/student");
const firestore = firebase.firestore();

const addStudent = async (req, res) => {
  try {
    const data = req.body;
    console.log(data)
      await firestore
        .collection("students")
        .add(data)
        .then((docRef) => {
          const link = {
            success: true,
            url: "http://94.237.65.225:8080/student/" + docRef.id,
          };
          res.send(link);
        });
  } catch (error) {
    res.status(400).send(error.message);
  }
};

const getAllStudent = async (req, res) => {
  try {
    const students = await firestore.collection("students");
    const data = await students.get();
    const studentArrays = [];
    if (data.empty) {
      res.status(400).send("0 record");
    } else {
      data.forEach((element) => {
        const student = new Student(
          element.id,
          element.data().name,
          element.data().birthday,
          element.data().address,
          element.data().phone
        );
        studentArrays.push(student);
      });
      res.render("getAllStudent", { students: studentArrays });
    }
  } catch (error) {
    res.status(400).send(error.message);
  }
};

const getStudent = async (req, res) => {
  try {
    const id = req.params.id;
    const student = await firestore.collection("students").doc(id);
    const data = await student.get();
    if (!data.exists) {
      res.status(400).send("student id: " + id + " don't exist");
    } else {
      const student = new Student(
        data.id,
        data.data().name,
        data.data().birthday,
        data.data().address,
        data.data().phone,
        data.data().status
      );
      res.render("getStudent", { student: student });
    }
  } catch (error) {
    res.status(400).send(error.message);
  }
};

const updateStudent = async (req, res) => {
  try {
    const id = req.params.id;
    const data = req.body;
    const student = await firestore.collection("students").doc(id);
    await student.update(data);
    res.redirect("/student/"+id);
  } catch (error) {
    res.status(400).send(error.message);
  }
};

const deleteStudent = async (req, res) => {
  try {
    const id = req.params.id;
    await firestore.collection("students").doc(id).delete();
    res.send("Delete id: " + id + " successfully");
  } catch (error) {
    res.status(400).send(error.message);
  }
};

module.exports = {
  addStudent,
  getAllStudent,
  getStudent,
  updateStudent,
  deleteStudent,
};
