const express = require("express");
const cors = require("cors");
const bodyParser = require("body-parser");
var multer = require('multer');
var upload = multer();
const config = require("./config");
const studentRoutes = require("./routes/student-routes");

const app = express();

app.set("view engine", "ejs");
app.use(express.json());
app.use(cors());
app.use(bodyParser.json());
app.use(upload.array()); 

app.use(studentRoutes.routes);

app.listen(config.port, () =>
  console.log(`App is listening on port: ${config.port}`)
);
