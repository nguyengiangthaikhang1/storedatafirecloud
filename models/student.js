class Student {
  constructor(id, name, birthday, address, phone,status) {
    this.id = id;
    this.name = name;
    this.birthday = birthday;
    this.address = address;
    this.phone = phone;
    this.status = status;
  }
}

module.exports = Student;
